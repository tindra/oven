// Amount form
$(document).ready(function() {
    $(document).on('click', function(e) {
        var target = $(e.target);
        var amountForm = "";

        if (e.target.dataset.component == "amount-form") {
            amountForm = target;
        } else if (target.parents('[data-component="amount-form"]').length) {
            amountForm = target.parents('[data-component="amount-form"]');
        }

        if (!amountForm) return;

        var
        amountInput = amountForm.find('[data-element="amount-input"]'),
        amountValue = amountInput.val(),
        amountValueNumber = +amountValue,
        amountMin = +amountInput.attr('data-min')  || 0,
        amountMax = +amountInput.attr('data-max'),
        amountStep = +amountInput.attr('data-step') || 1,
        amountNewValue = 0;

        amountInput.focus().val('').val(amountValue);

        if (target.attr('data-element') === 'inc-control' || target.attr('data-element') === 'dec-control') {
            if (target.attr('data-element') === 'inc-control') {
                amountForm.find('[data-element="dec-control"]').prop('disabled', false);
                if (amountMax && amountValueNumber >= amountMax) {
                    amountNewValue = amountValueNumber;
                    target.prop('disabled', true);
                } else {
                    amountNewValue = amountValueNumber + amountStep;
                }
                if (amountNewValue >= amountMax) {
                    target.prop('disabled', true);
                }
            } else if (target.attr('data-element') === 'dec-control') {
                amountForm.find('[data-element="inc-control"]').prop('disabled', false);
                if (amountValueNumber <= amountMin) {
                    amountNewValue = amountValueNumber;
                } else {
                    amountNewValue = amountValueNumber - amountStep;
                }
                if (amountNewValue <= amountMin) {
                    target.prop('disabled', true);
                }
            }
            if (Number.isInteger(amountStep)) {
                amountInput.val(amountNewValue);
            } else {
                amountInput.val(amountNewValue.toFixed(1));
            }
            amountInput.blur();
            amountInput.change();
        } else {
            amountInput.focus().val('').val(amountValue);
        }
    });
});
'use strict';

const activeClass    = 'is-active';
const collapsedClass = 'is-collapsed';
const disabledClass  = 'is-disabled';
const expandedClass  = 'is-expanded';
const filledClass    = 'is-filled';
const fixedClass     = 'is-fixed';
const focusClass     = 'is-focused';
const hoverClass     = 'is-hover';
const invisibleClass = 'is-invisible';
const selectedClass  = 'is-selected';
const scrolledClass  = 'is-scrolled';
const visibleClass   = 'is-visible';

const lockedScrollClass  = 'scroll-is-locked';
const mobileMenuVisibleClass = 'mobile-menu-is-visible';

const $body = $('.body');
const $main = $('.main');
const $footer = $('.footer');
const $header = $('[data-component="header"]');

const bpXS  = 320;
const bpSM  = 576;
const bpMD  = 768;
const bpLG  = 1024;
const bpXL  = 1240;
const bp2XL = 1440;

const gridOffset = 20;

document.addEventListener('DOMContentLoaded', function() {
    const $checkout = document.querySelector('[data-form="checkout"]');
    const $checkoutJquery = $('[data-form="checkout"]');

    if (!$checkout) {
        return false;
    }

    const $nextButtons = $checkout.querySelectorAll('[data-element="checkout-next"]');
    const $prevButtons = $checkout.querySelectorAll('[data-element="checkout-prev"]');
    const $editButtons = $checkout.querySelectorAll('[data-element="checkout-edit"]');
    const $submit = $checkout.querySelector('[data-element="checkout-submit"]');

    for (let i = 0; i < $nextButtons.length; i++) {
        $nextButtons[i].addEventListener("click", function(e) {
            e.preventDefault();

            const $button = this;
            const $section = $button.closest('[data-component="collapsible"]');
            const groupID = $section.getAttribute('id');
            const $sectionNext = $section.nextElementSibling;

            $checkoutJquery.parsley().validate({group:groupID, force: false});

            if ($checkoutJquery.parsley().isValid({group:groupID, force: false})) {
                $section.classList.add(collapsedClass, filledClass);
                $sectionNext.classList.remove(collapsedClass);
            }
        });
    }

    for (let i = 0; i < $prevButtons.length; i++) {
        $prevButtons[i].addEventListener("click", function(e) {
            e.preventDefault();

            const $button = this;
            const $section = $button.closest('[data-component="collapsible"]');
            const groupID = $section.getAttribute('id');
            const $sectionPrev = $section.previousElementSibling;

            $checkoutJquery.parsley().validate({group:groupID, force: false});

            $section.classList.add(collapsedClass);
            $sectionPrev.classList.remove(collapsedClass);
        });
    }

    for (let i = 0; i < $editButtons.length; i++) {
        $editButtons[i].addEventListener("click", function(e) {
            e.preventDefault();

            const $button = this;
            const $section = $button.closest('[data-component="collapsible"]');
            const groupID = $section.getAttribute('id');

            $checkout.querySelector('[data-component="collapsible"]:not(.'+collapsedClass+')').classList.add(collapsedClass);
            $section.classList.remove(collapsedClass, filledClass);
        });
    }

    $checkout.addEventListener("submit", function(e) {
        e.preventDefault();

        const $form = this;

        if ( $form.parsley().isValid() ) {
            const data = $form.serialize();
            console.log(data);
        }
    });
});
/* Collapsible */
document.addEventListener('DOMContentLoaded', function() {
    let $collapsibleToggles = document.querySelectorAll('[data-element="collapsible-toggle"]');
    let $collapsibleContents = document.querySelectorAll('[data-element="collapsible-content"]');
    let i = 0;

    if ($collapsibleToggles.length === 0) {
        return false;
    }

    responsiveCollapsibleHeight();

    for (i = 0; i < $collapsibleToggles.length; i++) {
        $collapsibleToggles[i].addEventListener("click", function(e) {
            let $toggle = this;
            let $toggleText = $toggle.querySelector('[data-element="collapsible-toggle-text"]');
            let toggleTextOn = $toggle.dataset.textOn || '';
            let toggleTextOff = $toggle.dataset.textOff || '';
            let toggleDisabledOn = $toggle.dataset.disabledOn;
            let windowWidth = window.innerWidth;

            if ($toggleText) {
                toggleTextOn = $toggleText.dataset.textOn;
                toggleTextOff = $toggleText.dataset.textOff;
            }

            if ((toggleDisabledOn === 'tablet' && windowWidth >= bpMD) || (toggleDisabledOn ==- 'desktop' && windowWidth >= bpXL)) {
                if ($toggle.hasAttribute('href')) {
                    window.location = this.href;
                } else {
                    e.preventDefault();
                }
            } else {
                e.preventDefault();

                let $collapsible = $toggle.closest('[data-component="collapsible"]');
                let $content     = $collapsible.querySelector('[data-element="collapsible-content"]');

                if ($toggleText) {
                    toggleText($toggleText, toggleTextOn, toggleTextOff);
                }

                if (toggleTextOn !== '' && !$toggleText) {
                    toggleText($toggle, toggleTextOn, toggleTextOff);
                }

                $content.style.maxHeight = $content.scrollHeight + 'px';
                $collapsible.classList.toggle(collapsedClass);
            }
        });
    }

    window.addEventListener("resize", function(e) {
        responsiveCollapsibleHeight();
    });
    window.addEventListener("orientationchange", function(e) {
        responsiveCollapsibleHeight();
    });

    function responsiveCollapsibleHeight() {
        let windowWidth = window.innerWidth;

        for (i = 0; i < $collapsibleContents.length; i++) {
            $collapsibleContents[i].style.maxHeight = '';
            $collapsibleContents[i].style.maxHeight = $collapsibleContents[i].scrollHeight + 'px';
        }
    }
});
/* Compare Items */
document.addEventListener('DOMContentLoaded', function() {
    let $compareItems = document.querySelectorAll('[data-compare-item]');

    if ($compareItems.length === 0) {
        return false;
    }

    let compareItemsCount = $compareItems.length;
    let compareItemsLabelsCount = $compareItems[0].querySelectorAll('[data-compare-label]').length;

    let labelsArray = [];
    let datasArray = [];


    for (let i = 1; i < compareItemsLabelsCount + 1; i++) {
        labelsArray.push(document.querySelectorAll('[data-compare-label="'+i+'"]'));
        datasArray.push(document.querySelectorAll('[data-compare-data="'+i+'"]'));
    }

    responsiveCompareItems(labelsArray);
    responsiveCompareItems(datasArray);

    window.addEventListener("resize", function(e) {
        responsiveCompareItems(labelsArray);
        responsiveCompareItems(datasArray);
    });
    window.addEventListener("orientationchange", function(e) {
        responsiveCompareItems(labelsArray);
        responsiveCompareItems(datasArray);
    });

    function responsiveCompareItems(array) {
        for (let i = 0; i < array.length; i++) {
            let els = [].slice.call(array[i]);

            let tallestEl = Math.max.apply(Math, els.map(function(el, index) {
                el.style.height = '';
                return el.offsetHeight;
            }));

            [].forEach.call(array[i], (el) => {
                el.style.height = tallestEl + 'px';
            });
        }
    }
});
/* Dropdown */
$(document).ready(function(){
    $('[data-element="dropdown-toggle"]').on('click', function(e){
        e.preventDefault();
    });

    $('[data-element="dropdown-toggle"]').each(function () {
        let tippyDropdown = tippy(this, {
            content(reference) {
                const id = reference.getAttribute('data-template');
                const template = document.getElementById(id);
                return template.innerHTML;
            },
            allowHTML: true,
            arrow: true,
            maxWidth: 250,
            theme: 'light',
            trigger: 'click',
            placement: 'bottom-start',
            interactive: true,
            appendTo: 'parent',
            offset: [0, 8],
            zIndex: 99,
            onShown: function(instance) {
                let $dropdownOption = $(instance.popper).find('[data-element="dropdown-option"]');
                let $dropdownList = $(instance.popper).find('[data-element="dropdown-list"]');
                let $dropdown = $dropdownList.closest('[data-component="dropdown"]');
                let $dropdownCheckAll = $(instance.popper).find('[data-element="dropdown-check-all"]');

                let ps = new PerfectScrollbar($dropdownList[0]);

                if ($dropdown.attr('data-type') === 'default' ) {
                    $dropdownOption.on('change', function(e){
                        let $option = $(this);
 
                        if ($option.prop('checked')) {
                            let optionText = $option.next('[data-element="dropdown-option-text"]').html();
                            let $toggleText = $dropdown.find('[data-element="dropdown-toggle-text"]');

                            $toggleText.html(optionText);

                            instance.hide();
                        }
                    });
                } else if ($dropdown.attr('data-type') === 'multiple' ) {
                    $dropdownCheckAll.on('click', function(e){
                        $dropdownOption.prop('checked', true).change();
                        //$dropdownOption.prop('checked', !$dropdownOption.prop('checked')).change();
                    });
                }
            }
        });
    });
});
Dropzone.autoDiscover = false;

$(document).ready(function () {
    let previewNode = $('[data-element="dropzone-template"]');
    let previewTemplate = previewNode.html();
    previewNode.parent().html('');

    $('[data-dropzone-block]').dropzone({
        maxFiles: 10,
        maxFilesize: 10,
        url: "/ajax_file_upload_handler/",
        previewTemplate: previewTemplate,
        autoProcessQueue:true,
        addRemoveLinks: false,
        //autoQueue: false, // Make sure the files aren't queued until manually added
        previewsContainer: '[data-element="dropzone-previews"]', // Define the container to display the previews
        clickable: '[data-element="dropzone-trigger"]', // Define the element that should be used as click trigger to select files.
        dictFileTooBig: 'File is over 10 Мb',
        init: function(){
            var myDropzone = this;
        },
        success: function (file, response) {
            $(file.previewElement).find('[data-dz-uploadprogress]').parent().hide();
        },
        error: function (file, response) {
            let item = $(file.previewElement);
            let itemError = $(file.previewElement).find('[data-dz-errormessage]');
            let itemName = $(file.previewElement).find('[data-dz-name]');

            if (file && response) {
                item.addClass('has-error');
                itemName.hide();
                itemError.html(response)
            } else {
                item.removeClass('has-error');
                itemName.show();
                itemError.html('')
            }
        },
        uploadprogress: function(progress) {
            $(file.previewElement).find('[data-dz-uploadprogress]').css('width', progress + '%');
        },
    });
});
/* Fav control */
(function() {
    const $favControls = document.querySelectorAll('[data-control="fav"]');

    if (!$favControls) {
        return false;
    }

    for (const favControl of $favControls) {
        favControl.addEventListener('click', function(e) {
            e.preventDefault();

            let control = this;

            control.classList.toggle(activeClass);
        });
    }
})();
feather.replace();
/* Filter */
document.addEventListener('DOMContentLoaded', function() {
    let $filterControls = document.querySelectorAll('[data-element="filter-control-input"]');
    let i;

    if (!$filterControls) {
        return false;
    }

    for (i = 0; i < $filterControls.length; i++) {
        $filterControls[i].addEventListener('change', function (e) {
            let $control = this;
            let $component = $control.closest('[data-component="filter-control"]');
            let tippyContent = '<div class="filter__tippy"><span class="filter__tippy-count text-md">Выбрано <b>120</b></span><button class="filter__tippy-button button button--primary button--sm">Показать</button></div>';
            let windowWidth = window.innerWidth;
            let tippyPlacement = "right";

            if (windowWidth < bpXL) {
                tippyPlacement = "top";
            }

            let $filterTippy = tippy($component, {
                content: tippyContent,
                appendTo: document.body,
                theme: 'light',
                placement: tippyPlacement,
                maxWidth: 250,
                arrow: false,
                hideOnClick: false,
                interactive: true,
                allowHTML: true,
                sticky: true,
                showOnCreate: true,
                onHidden(instance) {
                    instance.destroy();
                },
            });
        });
    }
});

/* header on scroll */
document.addEventListener('DOMContentLoaded', function() {
    const $header = document.querySelector('[data-component="header"]');

    if (!$header) {
        return false;
    }

    function headerDeafultScroll() {
        let onScroll = function () {
            $header.classList.toggle(scrolledClass, window.scrollY > $header.offsetHeight);
        };

        onScroll();
        window.addEventListener('scroll', function() {
            onScroll();
        });
    }

    headerDeafultScroll();
});
/* Hidden */
document.addEventListener('DOMContentLoaded', function() {
    let $hiddenToggles = document.querySelectorAll('[data-element="hidden-toggle"]');
    let $hiddenCloses = document.querySelectorAll('[data-element="hidden-close"]');

    if ($hiddenToggles.length === 0) {
        return false;
    }

    for (i = 0; i < $hiddenToggles.length; i++) {
        $hiddenToggles[i].addEventListener("click", function(e) {
            e.preventDefault();

            let $toggle  = this;
            let src      = $toggle.dataset.src;
            let $content = document.querySelector('[data-element="hidden-content"][data-src="'+src+'"]');

            $toggle.hidden = true;
            $content.hidden = false;
        });
    }

    for (i = 0; i < $hiddenCloses.length; i++) {
        $hiddenCloses[i].addEventListener("click", function(e) {
            e.preventDefault();

            let $close   = this;
            let $content = $close.closest('[data-element="hidden-content"]');
            let src      = $content.dataset.src;
            let $toggle  = document.querySelector('[data-element="hidden-toggle"][data-src="'+src+'"]');

            $toggle.hidden = false;
            $content.hidden = true;
        });
    }
});
let $phoneInputs = document.querySelectorAll('[data-mask="phone"]');

if ($phoneInputs.length !== 0) {
    for (const inputPhone of $phoneInputs) {
        Inputmask({"mask": "+9 (999) 999-99-99"}).mask(inputPhone);
    }
}

let $customInputs = document.querySelectorAll('[data-mask="custom"]');

if ($customInputs.length !== 0) {
    for (const inputCustom of $customInputs) {
        Inputmask().mask(inputCustom);
    }
}


jQuery(document).ready(function($){
    let $rangeSlider = $('[data-element="range-slider"]');

    $rangeSlider.each(function(){
        let $range = $(this);
        let $rangeComponent = $range.closest('[data-component="range"]');
        let $rangeInputFrom = $rangeComponent.find('[data-element="range-input-from"]');
        let $rangeInputTo = $rangeComponent.find('[data-element="range-input-to"]');
        let instance;

        $range.ionRangeSlider({
            type: "double",
            skin: 'round',
            hide_min_max: true,
            hide_from_to: true,
            grid: false,
            min: $range.attr('data-min'),
            max: $range.attr('data-max'),
            step: $range.attr('data-step'),
            onStart: function(data) {
                $rangeInputFrom.prop("value", data.from);
                $rangeInputTo.prop("value", data.to);
            },
            onChange: function(data) {
                $rangeInputFrom.prop("value", data.from);
                $rangeInputTo.prop("value", data.to);
            },
            onFinish: function (data) {
                $rangeInputFrom.change();
                $rangeInputTo.change();
            }
        });

        instance = $range.data("ionRangeSlider");

        $rangeInputFrom.on("input", function() {
            let val = $(this).prop("value");

            // validate
            if (val < instance.result.min) {
                val = instance.result.min;
            } else if (val > instance.result.max) {
                val = instance.result.max;
            }

            instance.update({
                from: val
            });
        });

        $rangeInputTo.on("input", function() {
            let val = $(this).prop("value");

            // validate
            if (val > instance.result.max) {
                val = instance.result.max;
            } else if (val < instance.result.from) {
                val = instance.result.from;
            }

            instance.update({
                to: val
            });
        });
    });
});
/* Mobile menu */
let $mobileMenu = $('[data-component="mobile-menu"]');
let $mobileMenuTrigger = $('[data-element="mobile-menu-trigger"]');
let $mobileMenuClose = $('[data-element="mobile-menu-close"]');

function hideMobileMenu() {
    $mobileMenuTrigger.removeClass(activeClass);
    $mobileMenu.removeClass(visibleClass);

    if ($body.hasClass(mobileMenuVisibleClass)) {
       $body.removeClass(mobileMenuVisibleClass);
       $body.removeClass(lockedScrollClass);
    }
}

function showMobileMenu(trigger, component) {
    if ($(window).width() < bpXL) {
        trigger.addClass(activeClass);
        component.addClass(visibleClass);
        $body.addClass(mobileMenuVisibleClass);
        $body.addClass(lockedScrollClass);
    }
}

function desktopCheck() {
    if ($(window).width() >= bpXL) {
        hideMobileMenu();
    }
}

$(document).ready(function(){

    $mobileMenuTrigger.on('click', function(e) {
        e.preventDefault();

        let $trigger = $(this);
        let triggerSrc = $trigger.attr('data-src');
        let $component = $('[data-component="mobile-menu"][data-src="'+ triggerSrc +'"]');

        if ($component.hasClass(visibleClass)) {
            hideMobileMenu();
        } else {
            $('[data-component="mobile-menu"]').removeClass(visibleClass);
            showMobileMenu($trigger, $component);
        }
    });

    $mobileMenuClose.on('click', function(e) {
        e.preventDefault();

        hideMobileMenu();
    });

    $(document).on('keyup', function(e) {
        if ($(window).width() < bpXL) {
            if (e.key === "Escape" || e.keyCode === 27) {
                hideMobileMenu();
            }
        }
    });

    $(document).on('click', function(e) {
        if ($(window).width() < bpXL) {
            if ($mobileMenu.has(e.target).length === 0 && !$mobileMenu.is(e.target) && $mobileMenuTrigger.has(e.target).length === 0 && !$mobileMenuTrigger.is(e.target)) {
                hideMobileMenu();
            }
        }
    });

    desktopCheck();

    $(window).on('orientationchange resize', function() {
        desktopCheck();
    });
});
/* Modal */
$(function() {
    $('[data-element="modal-trigger"]').magnificPopup({
        type: 'inline',
        fixedContentPos: true,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        callbacks: {
            beforeOpen: function() {
                let effectData = this.st.el.attr('data-effect');

                if (!effectData) {
                    effectData = 'mfp-zoom-in';
                }

                this.st.mainClass = effectData;
            },
            open: function() {
                $('.body').css('overflow', "hidden");
                let $mfp = $.magnificPopup.instance;
                let $trigger = $($mfp.currItem.el[0]);
                $trigger.attr('disabled', true);
            },
            close: function() {
                $('.body').css('overflow', "");
                let $mfp = $.magnificPopup.instance;
                let $trigger = $($mfp.currItem.el[0]);
                $trigger.removeAttr('disabled');
            }
        },
    });

    /* Gallery Modal */
    $('[data-element="gallery-trigger"]').magnificPopup({
        type: 'image',
        closeOnContentClick: false,
        mainClass: 'mfp-zoom-in',
        image: {
          verticalFit: true,
        },
        gallery: {
          enabled: true
        },
        zoom: {
          enabled: true,
          duration: 300, // don't foget to change the duration also in CSS
          opener: function(element) {
            return element.find('img');
          }
        },
        callbacks: {
            buildControls: function() {
                // re-appends controls inside the main container
                this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
            }
        }
    });

    $(document).on('click', '[data-element="modal-close"]', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
});

// Validation errors messages for Parsley
// Load this after Parsley
Parsley.addMessages('ru', {
    dateiso:  "Это значение должно быть корректной датой (ГГГГ-ММ-ДД).",
    minwords: "Это значение должно содержать не менее %s слов.",
    maxwords: "Это значение должно содержать не более %s слов.",
    words:    "Это значение должно содержать от %s до %s слов.",
    gt:       "Это значение должно быть больше.",
    gte:      "Это значение должно быть больше или равно.",
    lt:       "Это значение должно быть меньше.",
    lte:      "Это значение должно быть меньше или равно.",
    notequalto: "Это значение должно отличаться."
});

Parsley.addMessages('ru', {
    defaultMessage: "Некорректное значение.",
    type: {
        email:        "Введите адрес электронной почты.",
        url:          "Введите URL адрес.",
        number:       "Введите число.",
        integer:      "Введите целое число.",
        digits:       "Введите только цифры.",
        alphanum:     "Введите буквенно-цифровое значение."
    },
    notblank:       "Обязательное поле",
    required:       "Обязательное поле",
    pattern:        "Это значение некорректно.",
    min:            "Это значение должно быть не менее чем %s.",
    max:            "Это значение должно быть не более чем %s.",
    range:          "Это значение должно быть от %s до %s.",
    minlength:      "Это значение должно содержать не менее %s символов.",
    maxlength:      "Это значение должно содержать не более %s символов.",
    length:         "Это значение должно содержать от %s до %s символов.",
    mincheck:       "Выберите не менее %s значений.",
    maxcheck:       "Выберите не более %s значений.",
    check:          "Выберите от %s до %s значений.",
    equalto:        "Это значение должно совпадать."
});

Parsley.setLocale('ru');

/* Forms */
$(document).ready(function() {
    /* Select2 For parsley validation */
    $('[data-element="select"]').change(function() {
        $(this).trigger('input');
        $(this).parsley().validate();
    });

    /* Form Parsley Validation */
    let $parsleyForm = $('[data-parsley-validate]');
    let $parsleyFormSubmit = $parsleyForm.find('[type="submit"]');

    $parsleyForm.parsley({
        excluded: "[disabled], :hidden",
        errorClass: 'has-error',
        successClass: 'has-success',
        errorsWrapper: '<div class="form__errors-list"></div>',
        errorTemplate: '<div class="form__error"></div>',
        errorsContainer (field) {
            return field.$element.parent().closest('.form__group');
        },
        classHandler (field) {
            const $parent = field.$element.closest('.form__group');
            if ($parent.length) return $parent;

            return $parent;
        }
    });

    $('[data-parsley-validate]').on('submit', function(e) {
        e.preventDefault();

        const $form = $(this);
    });

});
/* Perfect scrollbar */
(function() {
    const psContainers = document.querySelectorAll('[data-perfect-scrollbar]');

    if (!psContainers) {
        return false;
    }

    for (const container of psContainers) {
        const ps = new PerfectScrollbar(container);
        const containerParent = container.parentElement;

        container.addEventListener('ps-y-reach-end', (event) => {
            containerParent.classList.remove('has-shadow');
        });

        container.addEventListener('ps-scroll-up', (event) => {
            containerParent.classList.add('has-shadow');
        });
    }
})();
/* Product options */
document.addEventListener('DOMContentLoaded', function() {
    let $productOptionsToggles = document.querySelectorAll('[data-element="product-options-toggle"]');
    let i = 0;

    if ($productOptionsToggles.length === 0) {
        return false;
    }

    for (i = 0; i < $productOptionsToggles.length; i++) {
        $productOptionsToggles[i].addEventListener("click", function(e) {
            e.stopPropagation();

            let $toggle = this;
            let $component = $toggle.closest('[data-component="product-options"]');

            $component.classList.toggle(expandedClass);
        });
    }
});

/* Select2 */
$(document).ready(function() {
    let selectCommonOptions = {
        dropdownCssClass: ':all:',
        containerCssClass: ':all:',
        minimumResultsForSearch: Infinity,
        width: '100%',
    };

    $('[data-element="select"]').select2(selectCommonOptions);
});
/* Show More/Less */
function toggleText(toggle, t1, t2){
    if (toggle.innerHTML == t1) {
        toggle.innerHTML = t2;
    } else {
        toggle.innerHTML = t1;
    }
    return this;
};

document.addEventListener('DOMContentLoaded', function() {
    let $showAllToggles = document.querySelectorAll('[data-element="show-all-toggle"]');
    let i;

    if (!$showAllToggles) {
        return false;
    }

    for (i = 0; i < $showAllToggles.length; i++) {
        $showAllToggles[i].addEventListener("click", function(e) {
            e.preventDefault();

            let $toggle = this;
            let toggleTextOn = $toggle.dataset.textOn;
            let toggleTextOff = $toggle.dataset.textOff;
            let $component = $toggle.closest('[data-component="show-all"]');
            let $collapsible = $component.closest('[data-element="collapsible-content"]');
            let $items = $component.querySelectorAll('[data-element="show-all-item"]');
            let j = 0;

            toggleText($toggle, toggleTextOn, toggleTextOff);

            if ($component.classList.contains(expandedClass)) {
                for (j = 0; j < $items.length; j++) {
                    $items[j].hidden = true;
                }

                if ($collapsible) {
                    $collapsible.style.maxHeight = $collapsible.scrollHeight + 'px';
                }

                $component.classList.remove(expandedClass);
            } else {
                for (j = 0; j < $items.length; j++) {
                    $items[j].hidden = false;
                }

                if ($collapsible) {
                    $collapsible.style.maxHeight = $collapsible.scrollHeight + 'px';
                }

                $component.classList.add(expandedClass);
            }
        });
    }
});

/* 2items auto slider */
(function(){
    let $2itemsAutoSliders = Array.from(document.querySelectorAll('[data-slider="2items-auto"]'));

    $2itemsAutoSliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $2itemsAutoSlider = new Swiper(slider, {
            slidesPerView: 2,
            spaceBetween: gridOffset,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
            breakpoints: {
                [bpXS]: {
                    slidesPerView: 'auto',
                },
                [bpXL]: {
                    slidesPerView: 2,
                },
            },
        });
    });
})();
/* 2items slider */
(function(){
    let $2itemsSliders = Array.from(document.querySelectorAll('[data-slider="2items"]'));

    $2itemsSliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $2itemsSlider = new Swiper(slider, {
            slidesPerView: 2,
            spaceBetween: gridOffset,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
            breakpoints: {
                [bpXS]: {
                    slidesPerView: 1,
                },
                [bpLG]: {
                    slidesPerView: 2,
                },
            },
        });
    });
})();
/* 4items slider */
(function(){
    let $4itemsSliders = Array.from(document.querySelectorAll('[data-slider="4items"]'));

    $4itemsSliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $4itemsSlider = new Swiper(slider, {
            slidesPerView: "auto",
            spaceBetween: gridOffset,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
            breakpoints: {
                [bpXL]: {
                    slidesPerView: 4,
                },
            },
        });
    });
})();
/* Menu slider */
(function(){
    let $menuSliders = Array.from(document.querySelectorAll('[data-slider="menu"]'));
    const destroyBreakpoint = window.matchMedia( '(min-width:'+bpLG+')' );

    if ($menuSliders.length === 0) {
        return false;
    }

    $menuSliders.forEach((slider) => {
        let activeSlideIndex = 0;

        if (slider.querySelector('.is-active')) {
            let $activeSlide = slider.querySelector('.is-active').parentElement;
            activeSlideIndex = [].indexOf.call($activeSlide.parentElement.children, $activeSlide);
        }

        let $slider;

        const breakpointChecker = function() {
            if ( destroyBreakpoint.matches === true ) {
                if ( $slider !== undefined ) $slider.destroy( true, true );
                return;
            } else if ( destroyBreakpoint.matches === false ) {
                return enableSwiper();
            }
        };

        const enableSwiper = function() {
            $slider = new Swiper (slider, {
                slidesPerView: "auto",
                spaceBetween: gridOffset,
                slideToClickedSlide: false,
                initialSlide: activeSlideIndex,
            });
        };

        destroyBreakpoint.addListener(breakpointChecker);

        breakpointChecker();
    });
})();
/* Navbar slider */
(function(){
    let $navbarSlider = new Swiper('[data-slider="navbar"]', {
        slidesPerView: "auto",
        spaceBetween: 16,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
})();
/* Product slider */
$(function() {
    let $productThumbs = new Swiper('[data-slider="product-thumbs"]', {
        spaceBetween: 8,
        slidesPerView: 'auto',
        direction: "vertical",
    });

    let $productSlider = new Swiper('[data-slider="product"]', {
        navigation: {
            nextEl: '.product__slider-next',
            prevEl: '.product__slider-prev',
        },
        thumbs: {
            swiper: $productThumbs,
        },
    });
});
/* Slider */
(function(){
    let $sliders = Array.from(document.querySelectorAll('[data-slider="slider"]'));

    $sliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $2itemsSlider = new Swiper(slider, {
            slidesPerView: 1,
            spaceBetween: gridOffset,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
        });
    });
})();
let scrollOptions = {
    speed: 1000,
    easing: 'easeInOutCubic',
    header: '[data-component="header"]',
    offset: 100,
};

let scroll = new SmoothScroll('[data-scroll]', scrollOptions);
/* Tabs */
document.addEventListener('DOMContentLoaded', function() {
    let $tabToggles = document.querySelectorAll('[data-element="tabs-toggle"]');
    let $tabTriggers = document.querySelectorAll('[data-element="tab-trigger"]');
    let $tabControls = document.querySelectorAll('[data-element="tabs-control"]');
    let i = 0;

    if ($tabToggles.length > 0 ) {
        for (i = 0; i < $tabToggles.length; i++) {
            $tabToggles[i].addEventListener("click", function(e) {
                e.preventDefault();

                let $toggle = this;
                let $tabs = $toggle.closest('[data-component="tabs"]');
                let tabID;

                if ($toggle.hasAttribute('href')) {
                    tabID = $toggle.getAttribute('href').slice(1);
                }

                if ($toggle.hasAttribute('data-src')) {
                    tabID = $toggle.dataset.src;
                }

                let $tabActive = '[data-element="tabs-tab"][data-id="' + tabID +'"]';

                if (!($toggle.classList.contains(activeClass))) {
                    $tabs.querySelector('.' + activeClass +'[data-element="tabs-tab"]').classList.remove(activeClass);
                    $tabs.querySelector('.' + activeClass +'[data-element="tabs-toggle"]').classList.remove(activeClass);
                    $tabs.querySelector($tabActive).classList.add(activeClass);
                    $toggle.classList.add(activeClass);
                }
            });
        }

        if ($tabTriggers.length > 0) {
            for (i = 0; i < $tabTriggers.length; i++) {
                $tabTriggers[i].addEventListener("click", function(e) {
                    e.preventDefault();

                    let $trigger = this;
                    let tabID = $trigger.getAttribute('href').slice(1);
                    let $tabSelected = document.querySelector('[data-element="tabs-tab"][data-id="' + tabID +'"]');
                    let $toggleSelected = document.querySelector('[data-element="tabs-toggle"][href="#' + tabID +'"]');
                    let $tabs = $toggleSelected.closest('[data-component="tabs"]');

                    if (!($toggleSelected.classList.contains(activeClass))) {
                        $tabs.querySelector('.' + activeClass +'[data-element="tabs-tab"]').classList.remove(activeClass);
                        $tabs.querySelector('.' + activeClass +'[data-element="tabs-toggle"]').classList.remove(activeClass);
                        $toggleSelected.classList.add(activeClass);
                        $tabSelected.classList.add(activeClass);
                    }

                    let tabScroll = new SmoothScroll();
                    tabScroll.animateScroll($toggleSelected, $trigger, scrollOptions);
                });
            }
        }
    }

    if ($tabControls.length > 0 ) {
        for (i = 0; i < $tabControls.length; i++) {
            $tabControls[i].addEventListener("click", function(e) {

                let $control = this;
                let $tabs = $control.closest('[data-component="tabs"]');
                let $input = $control.querySelector('input');
                let dependant = $input.dataset.dependant;
                let tabID;

                tabID = $control.dataset.id;

                let $tabActive = '[data-element="tabs-tab"][data-id="' + tabID +'"]';

                if (!($input.checked)) {
                    $tabs.querySelector('.' + activeClass +'[data-element="tabs-tab"]').classList.remove(activeClass);
                    $tabs.querySelector('.' + activeClass +'[data-element="tabs-control"]').classList.remove(activeClass);
                    $tabs.querySelector($tabActive).classList.add(activeClass);
                    $control.classList.add(activeClass);

                    if (dependant) {
                        let dependantID = $input.value;
                        let $dependantBlock = document.querySelector('[data-dependant-block][data-id="'+dependantID+'"]');
                        let $collapsibleContent = $dependantBlock.closest('[data-element="collapsible-content"]');
                        let groupID =  $dependantBlock.dataset.group;

                        document.querySelector('[data-group="'+groupID+'"][data-dependant-block]:not([hidden])').hidden = true;
                        $dependantBlock.hidden = false;

                        $collapsibleContent.style.maxHeight = '';
                        $collapsibleContent.style.maxHeight = $collapsibleContent.scrollHeight + 'px';
                    }
                }
            });
        }
    }
});
