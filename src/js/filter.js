/* Filter */
document.addEventListener('DOMContentLoaded', function() {
    let $filterControls = document.querySelectorAll('[data-element="filter-control-input"]');
    let i;

    if (!$filterControls) {
        return false;
    }

    for (i = 0; i < $filterControls.length; i++) {
        $filterControls[i].addEventListener('change', function (e) {
            let $control = this;
            let $component = $control.closest('[data-component="filter-control"]');
            let tippyContent = '<div class="filter__tippy"><span class="filter__tippy-count text-md">Выбрано <b>120</b></span><button class="filter__tippy-button button button--primary button--sm">Показать</button></div>';
            let windowWidth = window.innerWidth;
            let tippyPlacement = "right";

            if (windowWidth < bpXL) {
                tippyPlacement = "top";
            }

            let $filterTippy = tippy($component, {
                content: tippyContent,
                appendTo: document.body,
                theme: 'light',
                placement: tippyPlacement,
                maxWidth: 250,
                arrow: false,
                hideOnClick: false,
                interactive: true,
                allowHTML: true,
                sticky: true,
                showOnCreate: true,
                onHidden(instance) {
                    instance.destroy();
                },
            });
        });
    }
});
