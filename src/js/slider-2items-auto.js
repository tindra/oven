/* 2items auto slider */
(function(){
    let $2itemsAutoSliders = Array.from(document.querySelectorAll('[data-slider="2items-auto"]'));

    $2itemsAutoSliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $2itemsAutoSlider = new Swiper(slider, {
            slidesPerView: 2,
            spaceBetween: gridOffset,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
            breakpoints: {
                [bpXS]: {
                    slidesPerView: 'auto',
                },
                [bpXL]: {
                    slidesPerView: 2,
                },
            },
        });
    });
})();