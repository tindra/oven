/* Navbar slider */
(function(){
    let $navbarSlider = new Swiper('[data-slider="navbar"]', {
        slidesPerView: "auto",
        spaceBetween: 16,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
})();