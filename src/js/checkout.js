document.addEventListener('DOMContentLoaded', function() {
    const $checkout = document.querySelector('[data-form="checkout"]');
    const $checkoutJquery = $('[data-form="checkout"]');

    if (!$checkout) {
        return false;
    }

    const $nextButtons = $checkout.querySelectorAll('[data-element="checkout-next"]');
    const $prevButtons = $checkout.querySelectorAll('[data-element="checkout-prev"]');
    const $editButtons = $checkout.querySelectorAll('[data-element="checkout-edit"]');
    const $submit = $checkout.querySelector('[data-element="checkout-submit"]');

    for (let i = 0; i < $nextButtons.length; i++) {
        $nextButtons[i].addEventListener("click", function(e) {
            e.preventDefault();

            const $button = this;
            const $section = $button.closest('[data-component="collapsible"]');
            const groupID = $section.getAttribute('id');
            const $sectionNext = $section.nextElementSibling;

            $checkoutJquery.parsley().validate({group:groupID, force: false});

            if ($checkoutJquery.parsley().isValid({group:groupID, force: false})) {
                $section.classList.add(collapsedClass, filledClass);
                $sectionNext.classList.remove(collapsedClass);
            }
        });
    }

    for (let i = 0; i < $prevButtons.length; i++) {
        $prevButtons[i].addEventListener("click", function(e) {
            e.preventDefault();

            const $button = this;
            const $section = $button.closest('[data-component="collapsible"]');
            const groupID = $section.getAttribute('id');
            const $sectionPrev = $section.previousElementSibling;

            $checkoutJquery.parsley().validate({group:groupID, force: false});

            $section.classList.add(collapsedClass);
            $sectionPrev.classList.remove(collapsedClass);
        });
    }

    for (let i = 0; i < $editButtons.length; i++) {
        $editButtons[i].addEventListener("click", function(e) {
            e.preventDefault();

            const $button = this;
            const $section = $button.closest('[data-component="collapsible"]');
            const groupID = $section.getAttribute('id');

            $checkout.querySelector('[data-component="collapsible"]:not(.'+collapsedClass+')').classList.add(collapsedClass);
            $section.classList.remove(collapsedClass, filledClass);
        });
    }

    $checkout.addEventListener("submit", function(e) {
        e.preventDefault();

        const $form = this;

        if ( $form.parsley().isValid() ) {
            const data = $form.serialize();
            console.log(data);
        }
    });
});