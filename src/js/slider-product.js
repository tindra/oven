/* Product slider */
$(function() {
    let $productThumbs = new Swiper('[data-slider="product-thumbs"]', {
        spaceBetween: 8,
        slidesPerView: 'auto',
        direction: "vertical",
    });

    let $productSlider = new Swiper('[data-slider="product"]', {
        navigation: {
            nextEl: '.product__slider-next',
            prevEl: '.product__slider-prev',
        },
        thumbs: {
            swiper: $productThumbs,
        },
    });
});