/* 4items slider */
(function(){
    let $4itemsSliders = Array.from(document.querySelectorAll('[data-slider="4items"]'));

    $4itemsSliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $4itemsSlider = new Swiper(slider, {
            slidesPerView: "auto",
            spaceBetween: gridOffset,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
            breakpoints: {
                [bpXL]: {
                    slidesPerView: 4,
                },
            },
        });
    });
})();