/* 2items slider */
(function(){
    let $2itemsSliders = Array.from(document.querySelectorAll('[data-slider="2items"]'));

    $2itemsSliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $2itemsSlider = new Swiper(slider, {
            slidesPerView: 2,
            spaceBetween: gridOffset,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
            breakpoints: {
                [bpXS]: {
                    slidesPerView: 1,
                },
                [bpLG]: {
                    slidesPerView: 2,
                },
            },
        });
    });
})();