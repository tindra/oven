/* Menu slider */
(function(){
    let $menuSliders = Array.from(document.querySelectorAll('[data-slider="menu"]'));
    const destroyBreakpoint = window.matchMedia( '(min-width:'+bpLG+')' );

    if ($menuSliders.length === 0) {
        return false;
    }

    $menuSliders.forEach((slider) => {
        let activeSlideIndex = 0;

        if (slider.querySelector('.is-active')) {
            let $activeSlide = slider.querySelector('.is-active').parentElement;
            activeSlideIndex = [].indexOf.call($activeSlide.parentElement.children, $activeSlide);
        }

        let $slider;

        const breakpointChecker = function() {
            if ( destroyBreakpoint.matches === true ) {
                if ( $slider !== undefined ) $slider.destroy( true, true );
                return;
            } else if ( destroyBreakpoint.matches === false ) {
                return enableSwiper();
            }
        };

        const enableSwiper = function() {
            $slider = new Swiper (slider, {
                slidesPerView: "auto",
                spaceBetween: gridOffset,
                slideToClickedSlide: false,
                initialSlide: activeSlideIndex,
            });
        };

        destroyBreakpoint.addListener(breakpointChecker);

        breakpointChecker();
    });
})();