/* Dropdown */
$(document).ready(function(){
    $('[data-element="dropdown-toggle"]').on('click', function(e){
        e.preventDefault();
    });

    $('[data-element="dropdown-toggle"]').each(function () {
        let tippyDropdown = tippy(this, {
            content(reference) {
                const id = reference.getAttribute('data-template');
                const template = document.getElementById(id);
                return template.innerHTML;
            },
            allowHTML: true,
            arrow: true,
            maxWidth: 250,
            theme: 'light',
            trigger: 'click',
            placement: 'bottom-start',
            interactive: true,
            appendTo: 'parent',
            offset: [0, 8],
            zIndex: 99,
            onShown: function(instance) {
                let $dropdownOption = $(instance.popper).find('[data-element="dropdown-option"]');
                let $dropdownList = $(instance.popper).find('[data-element="dropdown-list"]');
                let $dropdown = $dropdownList.closest('[data-component="dropdown"]');
                let $dropdownCheckAll = $(instance.popper).find('[data-element="dropdown-check-all"]');

                let ps = new PerfectScrollbar($dropdownList[0]);

                if ($dropdown.attr('data-type') === 'default' ) {
                    $dropdownOption.on('change', function(e){
                        let $option = $(this);
 
                        if ($option.prop('checked')) {
                            let optionText = $option.next('[data-element="dropdown-option-text"]').html();
                            let $toggleText = $dropdown.find('[data-element="dropdown-toggle-text"]');

                            $toggleText.html(optionText);

                            instance.hide();
                        }
                    });
                } else if ($dropdown.attr('data-type') === 'multiple' ) {
                    $dropdownCheckAll.on('click', function(e){
                        $dropdownOption.prop('checked', true).change();
                        //$dropdownOption.prop('checked', !$dropdownOption.prop('checked')).change();
                    });
                }
            }
        });
    });
});