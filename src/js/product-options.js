/* Product options */
document.addEventListener('DOMContentLoaded', function() {
    let $productOptionsToggles = document.querySelectorAll('[data-element="product-options-toggle"]');
    let i = 0;

    if ($productOptionsToggles.length === 0) {
        return false;
    }

    for (i = 0; i < $productOptionsToggles.length; i++) {
        $productOptionsToggles[i].addEventListener("click", function(e) {
            e.stopPropagation();

            let $toggle = this;
            let $component = $toggle.closest('[data-component="product-options"]');

            $component.classList.toggle(expandedClass);
        });
    }
});
