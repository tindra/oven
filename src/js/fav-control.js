/* Fav control */
(function() {
    const $favControls = document.querySelectorAll('[data-control="fav"]');

    if (!$favControls) {
        return false;
    }

    for (const favControl of $favControls) {
        favControl.addEventListener('click', function(e) {
            e.preventDefault();

            let control = this;

            control.classList.toggle(activeClass);
        });
    }
})();