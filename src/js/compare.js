/* Compare Items */
document.addEventListener('DOMContentLoaded', function() {
    let $compareItems = document.querySelectorAll('[data-compare-item]');

    if ($compareItems.length === 0) {
        return false;
    }

    let compareItemsCount = $compareItems.length;
    let compareItemsLabelsCount = $compareItems[0].querySelectorAll('[data-compare-label]').length;

    let labelsArray = [];
    let datasArray = [];


    for (let i = 1; i < compareItemsLabelsCount + 1; i++) {
        labelsArray.push(document.querySelectorAll('[data-compare-label="'+i+'"]'));
        datasArray.push(document.querySelectorAll('[data-compare-data="'+i+'"]'));
    }

    responsiveCompareItems(labelsArray);
    responsiveCompareItems(datasArray);

    window.addEventListener("resize", function(e) {
        responsiveCompareItems(labelsArray);
        responsiveCompareItems(datasArray);
    });
    window.addEventListener("orientationchange", function(e) {
        responsiveCompareItems(labelsArray);
        responsiveCompareItems(datasArray);
    });

    function responsiveCompareItems(array) {
        for (let i = 0; i < array.length; i++) {
            let els = [].slice.call(array[i]);

            let tallestEl = Math.max.apply(Math, els.map(function(el, index) {
                el.style.height = '';
                return el.offsetHeight;
            }));

            [].forEach.call(array[i], (el) => {
                el.style.height = tallestEl + 'px';
            });
        }
    }
});