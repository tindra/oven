/* Show More/Less */
function toggleText(toggle, t1, t2){
    if (toggle.innerHTML == t1) {
        toggle.innerHTML = t2;
    } else {
        toggle.innerHTML = t1;
    }
    return this;
};

document.addEventListener('DOMContentLoaded', function() {
    let $showAllToggles = document.querySelectorAll('[data-element="show-all-toggle"]');
    let i;

    if (!$showAllToggles) {
        return false;
    }

    for (i = 0; i < $showAllToggles.length; i++) {
        $showAllToggles[i].addEventListener("click", function(e) {
            e.preventDefault();

            let $toggle = this;
            let toggleTextOn = $toggle.dataset.textOn;
            let toggleTextOff = $toggle.dataset.textOff;
            let $component = $toggle.closest('[data-component="show-all"]');
            let $collapsible = $component.closest('[data-element="collapsible-content"]');
            let $items = $component.querySelectorAll('[data-element="show-all-item"]');
            let j = 0;

            toggleText($toggle, toggleTextOn, toggleTextOff);

            if ($component.classList.contains(expandedClass)) {
                for (j = 0; j < $items.length; j++) {
                    $items[j].hidden = true;
                }

                if ($collapsible) {
                    $collapsible.style.maxHeight = $collapsible.scrollHeight + 'px';
                }

                $component.classList.remove(expandedClass);
            } else {
                for (j = 0; j < $items.length; j++) {
                    $items[j].hidden = false;
                }

                if ($collapsible) {
                    $collapsible.style.maxHeight = $collapsible.scrollHeight + 'px';
                }

                $component.classList.add(expandedClass);
            }
        });
    }
});
