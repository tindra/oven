/* header on scroll */
document.addEventListener('DOMContentLoaded', function() {
    const $header = document.querySelector('[data-component="header"]');

    if (!$header) {
        return false;
    }

    function headerDeafultScroll() {
        let onScroll = function () {
            $header.classList.toggle(scrolledClass, window.scrollY > $header.offsetHeight);
        };

        onScroll();
        window.addEventListener('scroll', function() {
            onScroll();
        });
    }

    headerDeafultScroll();
});