/* Perfect scrollbar */
(function() {
    const psContainers = document.querySelectorAll('[data-perfect-scrollbar]');

    if (!psContainers) {
        return false;
    }

    for (const container of psContainers) {
        const ps = new PerfectScrollbar(container);
        const containerParent = container.parentElement;

        container.addEventListener('ps-y-reach-end', (event) => {
            containerParent.classList.remove('has-shadow');
        });

        container.addEventListener('ps-scroll-up', (event) => {
            containerParent.classList.add('has-shadow');
        });
    }
})();