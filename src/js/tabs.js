/* Tabs */
document.addEventListener('DOMContentLoaded', function() {
    let $tabToggles = document.querySelectorAll('[data-element="tabs-toggle"]');
    let $tabTriggers = document.querySelectorAll('[data-element="tab-trigger"]');
    let $tabControls = document.querySelectorAll('[data-element="tabs-control"]');
    let i = 0;

    if ($tabToggles.length > 0 ) {
        for (i = 0; i < $tabToggles.length; i++) {
            $tabToggles[i].addEventListener("click", function(e) {
                e.preventDefault();

                let $toggle = this;
                let $tabs = $toggle.closest('[data-component="tabs"]');
                let tabID;

                if ($toggle.hasAttribute('href')) {
                    tabID = $toggle.getAttribute('href').slice(1);
                }

                if ($toggle.hasAttribute('data-src')) {
                    tabID = $toggle.dataset.src;
                }

                let $tabActive = '[data-element="tabs-tab"][data-id="' + tabID +'"]';

                if (!($toggle.classList.contains(activeClass))) {
                    $tabs.querySelector('.' + activeClass +'[data-element="tabs-tab"]').classList.remove(activeClass);
                    $tabs.querySelector('.' + activeClass +'[data-element="tabs-toggle"]').classList.remove(activeClass);
                    $tabs.querySelector($tabActive).classList.add(activeClass);
                    $toggle.classList.add(activeClass);
                }
            });
        }

        if ($tabTriggers.length > 0) {
            for (i = 0; i < $tabTriggers.length; i++) {
                $tabTriggers[i].addEventListener("click", function(e) {
                    e.preventDefault();

                    let $trigger = this;
                    let tabID = $trigger.getAttribute('href').slice(1);
                    let $tabSelected = document.querySelector('[data-element="tabs-tab"][data-id="' + tabID +'"]');
                    let $toggleSelected = document.querySelector('[data-element="tabs-toggle"][href="#' + tabID +'"]');
                    let $tabs = $toggleSelected.closest('[data-component="tabs"]');

                    if (!($toggleSelected.classList.contains(activeClass))) {
                        $tabs.querySelector('.' + activeClass +'[data-element="tabs-tab"]').classList.remove(activeClass);
                        $tabs.querySelector('.' + activeClass +'[data-element="tabs-toggle"]').classList.remove(activeClass);
                        $toggleSelected.classList.add(activeClass);
                        $tabSelected.classList.add(activeClass);
                    }

                    let tabScroll = new SmoothScroll();
                    tabScroll.animateScroll($toggleSelected, $trigger, scrollOptions);
                });
            }
        }
    }

    if ($tabControls.length > 0 ) {
        for (i = 0; i < $tabControls.length; i++) {
            $tabControls[i].addEventListener("click", function(e) {

                let $control = this;
                let $tabs = $control.closest('[data-component="tabs"]');
                let $input = $control.querySelector('input');
                let dependant = $input.dataset.dependant;
                let tabID;

                tabID = $control.dataset.id;

                let $tabActive = '[data-element="tabs-tab"][data-id="' + tabID +'"]';

                if (!($input.checked)) {
                    $tabs.querySelector('.' + activeClass +'[data-element="tabs-tab"]').classList.remove(activeClass);
                    $tabs.querySelector('.' + activeClass +'[data-element="tabs-control"]').classList.remove(activeClass);
                    $tabs.querySelector($tabActive).classList.add(activeClass);
                    $control.classList.add(activeClass);

                    if (dependant) {
                        let dependantID = $input.value;
                        let $dependantBlock = document.querySelector('[data-dependant-block][data-id="'+dependantID+'"]');
                        let $collapsibleContent = $dependantBlock.closest('[data-element="collapsible-content"]');
                        let groupID =  $dependantBlock.dataset.group;

                        document.querySelector('[data-group="'+groupID+'"][data-dependant-block]:not([hidden])').hidden = true;
                        $dependantBlock.hidden = false;

                        $collapsibleContent.style.maxHeight = '';
                        $collapsibleContent.style.maxHeight = $collapsibleContent.scrollHeight + 'px';
                    }
                }
            });
        }
    }
});
