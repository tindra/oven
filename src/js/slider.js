/* Slider */
(function(){
    let $sliders = Array.from(document.querySelectorAll('[data-slider="slider"]'));

    $sliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $2itemsSlider = new Swiper(slider, {
            slidesPerView: 1,
            spaceBetween: gridOffset,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
        });
    });
})();