/* Hidden */
document.addEventListener('DOMContentLoaded', function() {
    let $hiddenToggles = document.querySelectorAll('[data-element="hidden-toggle"]');
    let $hiddenCloses = document.querySelectorAll('[data-element="hidden-close"]');

    if ($hiddenToggles.length === 0) {
        return false;
    }

    for (i = 0; i < $hiddenToggles.length; i++) {
        $hiddenToggles[i].addEventListener("click", function(e) {
            e.preventDefault();

            let $toggle  = this;
            let src      = $toggle.dataset.src;
            let $content = document.querySelector('[data-element="hidden-content"][data-src="'+src+'"]');

            $toggle.hidden = true;
            $content.hidden = false;
        });
    }

    for (i = 0; i < $hiddenCloses.length; i++) {
        $hiddenCloses[i].addEventListener("click", function(e) {
            e.preventDefault();

            let $close   = this;
            let $content = $close.closest('[data-element="hidden-content"]');
            let src      = $content.dataset.src;
            let $toggle  = document.querySelector('[data-element="hidden-toggle"][data-src="'+src+'"]');

            $toggle.hidden = false;
            $content.hidden = true;
        });
    }
});