let $phoneInputs = document.querySelectorAll('[data-mask="phone"]');

if ($phoneInputs.length !== 0) {
    for (const inputPhone of $phoneInputs) {
        Inputmask({"mask": "+9 (999) 999-99-99"}).mask(inputPhone);
    }
}

let $customInputs = document.querySelectorAll('[data-mask="custom"]');

if ($customInputs.length !== 0) {
    for (const inputCustom of $customInputs) {
        Inputmask().mask(inputCustom);
    }
}

